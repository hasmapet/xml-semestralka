COUNTRIES = xml/romania.xml xml/ireland.xml xml/tunisia.xml xml/montenegro.xml
COUNTRIES_HTML = html/romania.html html/ireland.html html/tunisia.html html/montenegro.html

all: countries.xml html countries.pdf hasmapet.zip

validate_rnc: $(COUNTRIES) validation/country.rnc
	jing -c validation/country.rnc $(COUNTRIES)

validate_dtd: $(COUNTRIES) validation/country.dtd
	xmllint --noout --dtdvalid validation/country.dtd $(COUNTRIES)

validate: $(COUNTRIES) validation/country.dtd validation/country.rnc
	xmllint --noout --dtdvalid validation/country.dtd $(COUNTRIES)
	jing -c validation/country.rnc $(COUNTRIES)

countries.xml: $(COUNTRIES) join.dtd
	xmllint --dropdtd --noent join.dtd > xml/countries.xml


index.html: xml/countries.xml xslt/index.xslt
	saxon xml/countries.xml xslt/index.xslt > html/index.html

countries.html: $(COUNTRIES) xslt/country.xslt
	saxon xml/countries.xml xslt/country.xslt

html: xslt/country.xslt xml/countries.xml xslt/index.xslt
	saxon xml/countries.xml xslt/index.xslt > html/index.html
	saxon xml/countries.xml xslt/country.xslt

countries.fo: xml/countries.xml xslt/pdf.xslt
	saxon xml/countries.xml xslt/pdf.xslt > pdf/countries.fo

countries.pdf: countries.fo xslt/pdf.xslt
	fop -fo pdf/countries.fo -pdf pdf/countries.pdf

hasmapet.zip: countries.xml index.html countries.pdf README.md
	rm -rf hasmapet.zip
	zip -r hasmapet.zip html pdf validation xslt Makefile xslt README.md -x *.git*

clean:
	rm -rf pdf/*.* html/*.html xml/countries.xml
