needed tools and software:
jing
xmllint
saxon
fop

How it works:
make all
    - creates all html and pdf outputs and zips needed contents

validate_rnc
    - validates all xml files with RelaxNG by validation/country.rnc

validate_dtd
    - validates all xml files with DTD by validation/country.dtd

validate
    - validates all xml files with both DTD and RelaxNG

countries.xml
    - joins all separate xml files with join.dtd to one stored in xml/countries.xml

index.html
    - creates index.html

countries.html
    - creates separate html files for countries

html
    -creates all html outputs

countries.fo
    -creates pdf/countries.fo for creating pdf

countries.pdf
    -creates output PDF file pdf/countries.pdf

hasmapet.zip
    - zips all generated files

clean
    - deletes all generated files
