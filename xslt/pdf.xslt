<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:fo="http://www.w3.org/1999/XSL/Format">
    <xsl:output method="xml" indent="yes"/>
    <xsl:template match="/">
        <fo:root>
            <fo:layout-master-set>
                <fo:simple-page-master master-name="A4-portrait" page-height="29.7cm" page-width="21.0cm" margin="2cm"
                                       margin-bottom="1cm">
                    <fo:region-body/>
                    <fo:region-after/>
                </fo:simple-page-master>
            </fo:layout-master-set>
            <fo:declarations>
                <x:xmpmeta xmlns:x="adobe:ns:meta/">
                    <rdf:RDF xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
                        <rdf:Description rdf:about="" xmlns:dc="http://purl.org/dc/elements/1.1/">
                            <!-- Dublin Core properties go here -->
                            <dc:title>World Factbook</dc:title>
                        </rdf:Description>
                    </rdf:RDF>
                </x:xmpmeta>
            </fo:declarations>
            <fo:page-sequence master-reference="A4-portrait">
                <fo:static-content flow-name="xsl-region-after">
                    <fo:block text-align="center">
                        Page
                        <fo:page-number/>
                    </fo:block>
                </fo:static-content>

                <fo:flow flow-name="xsl-region-body">
                    <fo:block font-size="18pt" font-weight="500" text-align="center">
                        World Factbook
                    </fo:block>
                    <xsl:apply-templates/>
                </fo:flow>

            </fo:page-sequence>

        </fo:root>
    </xsl:template>

    <xsl:template match="countries">
        <fo:block>
            Available countries:
        </fo:block>
        <fo:list-block provisional-distance-between-starts="0.3cm" provisional-label-separation="0.15cm">
            <xsl:for-each select="country">
                <fo:list-item>
                    <fo:list-item-label end-indent="label-end()">
                        <fo:block>
                            <fo:inline>&#183;</fo:inline>
                        </fo:block>
                    </fo:list-item-label>
                    <fo:list-item-body start-indent="body-start()">
                        <fo:block>
                            <fo:basic-link internal-destination="{@id}" color="blue" text-decoration="underline">
                                <xsl:value-of select="@name"/>
                            </fo:basic-link>

                            <fo:list-block provisional-distance-between-starts="0.3cm"
                                           provisional-label-separation="0.15cm"
                                           margin-left="15pt">

                                <fo:list-item>
                                    <fo:list-item-label end-indent="label-end()">
                                        <fo:block>
                                            <fo:inline>&#183;</fo:inline>
                                        </fo:block>
                                    </fo:list-item-label>
                                    <fo:list-item-body start-indent="body-start()">
                                        <fo:block>
                                            <fo:basic-link internal-destination="{generate-id(pictures)}" color="blue"
                                                           text-decoration="underline">
                                                Pictures
                                            </fo:basic-link>
                                        </fo:block>
                                    </fo:list-item-body>
                                </fo:list-item>

                                <xsl:for-each select="section">
                                    <fo:list-item>
                                        <fo:list-item-label end-indent="label-end()">
                                            <fo:block>
                                                <fo:inline>&#183;</fo:inline>
                                            </fo:block>
                                        </fo:list-item-label>
                                        <fo:list-item-body start-indent="body-start()">
                                            <fo:block>
                                                <fo:basic-link internal-destination="{generate-id(.)}" color="blue"
                                                               text-decoration="underline">
                                                    <xsl:value-of select="@name"/>
                                                </fo:basic-link>
                                            </fo:block>
                                        </fo:list-item-body>
                                    </fo:list-item>
                                </xsl:for-each>
                            </fo:list-block>

                        </fo:block>
                    </fo:list-item-body>
                </fo:list-item>
            </xsl:for-each>
        </fo:list-block>
        <xsl:apply-templates select="country"/>
    </xsl:template>

    <xsl:template match="country">
        <fo:block page-break-before="always"/>
        <fo:block font-size="15pt" font-weight="500" text-align="center">
            <xsl:attribute name="id">
                <xsl:value-of select="@id"/>
            </xsl:attribute>
            <xsl:value-of select="@name"/>
        </fo:block>

        <fo:block padding-top="15pt">
            <fo:block font-size="15pt" font-weight="500" text-align="left">
                Introduction
            </fo:block>
            <xsl:apply-templates select="introduction/background"/>
        </fo:block>

        <fo:block font-size="15pt" font-weight="400" text-align="center" margin-top="30pt" id="{generate-id(pictures)}">
            Pictures
        </fo:block>

        <xsl:apply-templates select="pictures/picture"/>
        <xsl:apply-templates select="photos/photo"/>

        <xsl:apply-templates select="section"/>
    </xsl:template>

    <xsl:template match="introduction/background">

            <fo:block margin-bottom="8pt" text-indent="15pt">
                <xsl:value-of select="."/>
            </fo:block>
    </xsl:template>

    <xsl:template match="picture">
        <fo:block text-align="center" padding-top="15pt">
            <fo:block>
                <fo:external-graphic height="200pt"
                                     content-width="scale-to-fit"
                                     scaling="uniform">
                    <xsl:attribute name="src">
                        url('<xsl:value-of select="@url"/>')
                    </xsl:attribute>
                </fo:external-graphic>
            </fo:block>
        </fo:block>
    </xsl:template>

    <xsl:template match="photo">
        <fo:block text-align="center" padding-top="15pt">
            <fo:block>
                <fo:external-graphic height="200pt"
                                     content-width="scale-to-fit"
                                     scaling="uniform">
                    <xsl:attribute name="src">
                        url('<xsl:value-of select="@url"/>')
                    </xsl:attribute>
                </fo:external-graphic>
            </fo:block>
        </fo:block>
    </xsl:template>

    <xsl:template match="section">
        <fo:block font-size="15pt" font-weight="400" text-align="center" margin-top="30pt" id="{generate-id(.)}">
            <xsl:value-of select="@name"/>
        </fo:block>

        <xsl:apply-templates select="sub-section"/>
    </xsl:template>

    <xsl:template match="sub-section">
        <fo:block margin-top="5pt">
            <fo:inline font-weight="600">
                <xsl:value-of select="@name"/>
            </fo:inline>
            :
            <fo:inline margin-left="15pt">
                <xsl:apply-templates/>
            </fo:inline>
        </fo:block>
    </xsl:template>

    <xsl:template match="parameter-list">
        <fo:block margin-top="5pt">
            <fo:inline font-weight="600">
                <xsl:value-of select="@name"/>
            </fo:inline>
            :
            <fo:block margin-left="15pt">
                <fo:list-block provisional-distance-between-starts="0.3cm" provisional-label-separation="0.15cm">
                    <xsl:apply-templates select="parameter-item"/>
                </fo:list-block>
            </fo:block>
        </fo:block>
    </xsl:template>

    <xsl:template match="parameter-item">
        <fo:list-item>
            <fo:list-item-label end-indent="label-end()">
                <fo:block>
                    <fo:inline>&#183;</fo:inline>
                </fo:block>
            </fo:list-item-label>
            <fo:list-item-body start-indent="body-start()">
                <fo:block>
                    <fo:inline font-weight="600">
                        <xsl:value-of select="@name"/>:
                    </fo:inline>
                    <xsl:value-of select="."/>
                </fo:block>
            </fo:list-item-body>
        </fo:list-item>
    </xsl:template>

    <xsl:template match="parameter">
        <fo:block margin-top="5pt">
            <fo:inline font-weight="600">
                <xsl:value-of select="@name"/>
            </fo:inline>
            :
            <fo:block margin-left="15pt">
                <xsl:value-of select="."/>
            </fo:block>
        </fo:block>
    </xsl:template>

    <xsl:template match="paragraph">
            <fo:block margin-left="15pt">
                <xsl:value-of select="."/>
            </fo:block>
    </xsl:template>

</xsl:stylesheet>
