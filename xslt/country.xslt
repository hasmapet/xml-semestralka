<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output method="html" indent="yes"/>
    <xsl:template match="/countries">
        <xsl:for-each select="country">
            <xsl:result-document method="html" href="html/{@id}.html" indent="yes">
                <xsl:apply-templates select="."/>
            </xsl:result-document>
        </xsl:for-each>
    </xsl:template>
    <xsl:template match="country">
        <html>
            <head>
                <title>
                    <xsl:value-of select="@name"/>
                </title>
                <link rel="stylesheet" href="style.css" />
                <link rel="stylesheet" href='https://cdn.jsdelivr.net/npm/lightgallery@1.6.12/dist/css/lightgallery.min.css' crossorigin="anonymous" />
                <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons" />
                <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous" />
                <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
                <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
                <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
                <script src='https://cdn.jsdelivr.net/npm/lightgallery@1.6.12/dist/js/lightgallery.min.js'></script>
                <script src="js/lg-thumbnail.min.js"></script>
                <script src="js/lg-fullscreen.min.js"></script>
            </head>
            <body>
                <nav class="navbar navbar-dark bg-dark navbar-expand-lg mb-5">
                  <a class="nav-link navbar-brand" href="index.html">Countries</a>
                  <div class="collapse navbar-collapse">
                      <div class="navbar-nav">
                          <xsl:for-each select="../country">
                            <a class="nav-item nav-link">
                                <xsl:attribute name="href">
                                    <xsl:value-of select="@id" />
                                    <xsl:text>.html</xsl:text>
                                </xsl:attribute>
                                <xsl:value-of select="@name" />
                            </a>
                          </xsl:for-each>
                      </div>
                    </div>
                </nav>
                <div class="container rounded shadow mb-5">
                    <div class="section">
                        <div class="container text-center pb-5 ">
                            <h1 class="display-3">
                                <xsl:value-of select="@name"/>
                            </h1>
                        </div>
                        <div class="container p-0 pl-1">
                            <h2 class="mb-2">
                                Introduction
                            </h2>
                            <xsl:apply-templates select="introduction/background"/>
                        </div>
                        <div class="accordion" id="accordion">
                            <xsl:apply-templates select="pictures"/>
                            <xsl:apply-templates select="section"/>
                        </div>
                    </div>
                </div>
                <script type="text/javascript">
                    $(document).ready(function() {
                        $("#lightgallery").lightGallery();
                    });
                </script>
            </body>
        </html>
    </xsl:template>

    <xsl:template match="introduction/background">
            <p>
                <xsl:value-of select="."/>
            </p>
    </xsl:template>

    <xsl:template match="pictures">
        <div class="card">
            <div id="headingPictures" class="card-header">
                <h2 class="mb-0">
                  <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#pictures" aria-controls="pictures">
                      <i class="material-icons float-left mr-2">photo_library</i>
                      Photos
                  </button>
                </h2>
            </div>
            <div id="pictures" class="collapse" data-parent="#accordion" aria-labelledby="headingPictures">
                <div class="card-body">
                    <div id="lightgallery">
                        <xsl:apply-templates select="picture"/>
                        <xsl:apply-templates select="../photos/photo"/>
                    </div>
                </div>
            </div>
        </div>
    </xsl:template>

    <xsl:template match="picture">
        <a style="overflow: hidden" class="ml-4">
            <xsl:attribute name="href">
                <xsl:value-of select="@url"/>
            </xsl:attribute>
            <img>
                <xsl:attribute name="src">
                    <xsl:value-of select="@url"/>
                </xsl:attribute>
                <xsl:attribute name="alt">
                    <xsl:value-of select="." />
                </xsl:attribute>
            </img>
        </a>
    </xsl:template>

    <xsl:template match="photo">
        <a style="overflow: hidden" class="ml-4">
            <xsl:attribute name="href">
                <xsl:value-of select="@url"/>
            </xsl:attribute>
            <img class=" mb-4">
                <xsl:attribute name="src">
                    <xsl:value-of select="@url"/>
                </xsl:attribute>
                <xsl:attribute name="alt">
                    <xsl:value-of select="." />
                </xsl:attribute>
            </img>
        </a>
    </xsl:template>


    <xsl:template match="section">
        <div class="card">
            <div class="card-header">
                <xsl:attribute name="id">
                    <xsl:text>heading</xsl:text>
                    <xsl:value-of select="@id" />
                </xsl:attribute>
                <h2 class="mb-0">
                  <button class="btn btn-link" type="button" data-toggle="collapse">
                      <xsl:attribute name="data-target">
                          <xsl:text>#</xsl:text>
                          <xsl:value-of select="@id" />
                      </xsl:attribute>
                      <xsl:attribute name="aria-controls">
                          <xsl:value-of select="@name" />
                      </xsl:attribute>
                      <i class="material-icons float-left mr-2">
                          <xsl:choose>
                              <xsl:when test="@name = 'Geography'">
                                  public
                              </xsl:when>
                              <xsl:when test="@name = 'Economy'">
                                  attach_money
                              </xsl:when>
                              <xsl:when test="@name = 'Energy'">
                                  power
                              </xsl:when>
                              <xsl:when test="@name = 'Communications'">
                                  local_phone
                              </xsl:when>
                              <xsl:when test="@name = 'Transnational issues'">
                                  priority_high
                              </xsl:when>
                              <xsl:when test="@name = 'People and society'">
                                  people
                              </xsl:when>
                              <xsl:when test="@name = 'Transportation'">
                                  train
                              </xsl:when>
                              <xsl:when test="@name = 'Government'">
                                  assignment
                              </xsl:when>
                              <xsl:when test="@name = 'Military and security'">
                                  security
                              </xsl:when>
                              <xsl:otherwise>
                                  wrap_text
                              </xsl:otherwise>
                          </xsl:choose>
                      </i>
                    <xsl:value-of select="@name"/>
                  </button>
                </h2>
            </div>
            <div class="collapse" data-parent="#accordion">
                <xsl:attribute name="id">
                    <xsl:value-of select="@id" />
                </xsl:attribute>
                <xsl:attribute name="aria-labelledby">
                    <xsl:text>heading</xsl:text>
                    <xsl:value-of select="@id" />
                </xsl:attribute>
              <div class="card-body">
                    <xsl:apply-templates/>
                </div>
            </div>
        </div>
    </xsl:template>

    <xsl:template match="sub-section">
        <div>
            <h4>
                <xsl:value-of select="@name"/>
            </h4>
        </div>
        <div class="pb-2 pl-2">
            <xsl:apply-templates/>
        </div>
    </xsl:template>

    <xsl:template match="parameter">
    <div>
        <h6 class="float-left mb-0">
            <strong>
                <xsl:value-of select="@name"/>: &#160;
            </strong>
        </h6>
        <xsl:value-of select="."/><br/>
    </div>
    </xsl:template>

    <xsl:template match="parameter-list">
        <h6 class="float-left mb-0">
            <strong>
                <xsl:value-of select="@name"/>
                :
            </strong>
        </h6><br/>
            <ul class="browser-default">
                <xsl:apply-templates select="parameter-item"/>
            </ul>
    </xsl:template>

    <xsl:template match="parameter-item">
        <h6 class="float-left mb-0">
            <strong>
                <xsl:value-of select="@name"/>: &#160;
            </strong>
        </h6>
        <xsl:value-of select="."/><br/>

    </xsl:template>

    <xsl:template match="paragraph">
        <p class="pb-0">
            <xsl:value-of select="."/>
        </p>
    </xsl:template>

</xsl:stylesheet>
