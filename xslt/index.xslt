<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output method="html" indent="yes"/>
    <xsl:template match="/countries">
        <html>
            <head>
                <title>Countries</title>

                <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous" />
                <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
                <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
                <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
                <link rel="stylesheet" href="style.css" />
            </head>
            <body>
                <nav class="navbar navbar-dark bg-dark navbar-expand-lg mb-5">
                  <a class="nav-link active navbar-brand" href="#">Countries</a>
                  <div class="collapse navbar-collapse">
                      <div class="navbar-nav">
                          <xsl:for-each select="country">
                            <a class="nav-item nav-link">
                                <xsl:attribute name="href">
                                    <xsl:value-of select="@id" />
                                    <xsl:text>.html</xsl:text>
                                </xsl:attribute>
                                <xsl:value-of select="@name" />
                            </a>
                          </xsl:for-each>
                      </div>
                    </div>
                </nav>
                <div class="container shadow rounded">
                    <div class="container text-center pb-5 ">
                        <h1 class="display-3">Select country</h1>
                    </div>
                    <div class="">
                        <div class="container card border-bottom-0">
                            <xsl:apply-templates select="country"/>
                        </div>
                    </div>
                </div>
            </body>
        </html>
    </xsl:template>

    <xsl:template match="country">
        <a class="collection-item text-secondary">
            <xsl:attribute name="href">
                <xsl:value-of select="@id" />
                <xsl:text>.html</xsl:text>
            </xsl:attribute>
            <div class="card-body border-bottom">
                <div class="row">
                    <div class="col-sm">
                        <xsl:apply-templates select="pictures/picture[@name='flag']"/>
                    </div>
                    <div class="col-sm align-middle my-auto">
                        <xsl:value-of select="@name" />
                    </div>
                </div>
            </div>
        </a>
    </xsl:template>

    <xsl:template match="pictures/picture[@name='flag']">
        <img class="thumbnail float-right">
            <xsl:attribute name="src">
                <xsl:value-of select="@url" />
            </xsl:attribute>
            <xsl:attribute name="alt">
                <xsl:value-of select="." />
            </xsl:attribute>
        </img>
    </xsl:template>
</xsl:stylesheet>
